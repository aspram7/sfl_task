const searchEl = document.querySelector(".search-input");
const paginationPrev = document.querySelectorAll(".prev");
const paginationNext = document.querySelectorAll(".next");
const storageSearchValue = localStorage.getItem("search-repos");
const PER_PAGE = 20;
let page = localStorage.getItem("page-repos") || 1;
let totalCount = 0;

if (storageSearchValue) {
  searchEl.value = storageSearchValue;
  getRepos(storageSearchValue);
}

function searchChange(e) {
  localStorage.setItem("search-repos", e.target.value);
}

function onSearchRepos(e) {
  if (e.keyCode === 13) {
    const { value } = e.target;
    page = 1;
    localStorage.setItem("page-repos", page);
    getRepos(value);
  }
}

async function getRepos(value) {
  const reposData = await fetch(
    `https://api.github.com/search/repositories?q=${value}&per_page=${PER_PAGE}&page=${page}`
  ).then((res) => res.json());

  totalCount = reposData.total_count;
  handlePagination();
  const reposList = document.querySelector(".repos-list");
  reposList.innerHTML = "";
  const fragment = new DocumentFragment();

  if (reposData.items) {
    reposData.items.forEach((repo) => {
      const div = document.createElement("div");
      div.className = "repo-item";
      const repoElement = `
      <p>${repo.name}</p>
      <a href="${repo.html_url}" target="_blank">${repo.html_url}</a>
      `;
      div.innerHTML = repoElement;
      fragment.appendChild(div);
    });

    reposList.appendChild(fragment);
  }
}

function navigatePage(n) {
  const searchValue = localStorage.getItem("search-repos");
  page = +page + n;
  localStorage.setItem("page-repos", page);
  handlePagination();
  getRepos(searchValue);
}

function handlePagination() {
  if (page > 1) {
    paginationPrev[0].classList.remove("disabled");
    paginationPrev[1].classList.remove("disabled");
  } else {
    paginationPrev[0].classList.add("disabled");
    paginationPrev[1].classList.add("disabled");
  }

  if (totalCount / PER_PAGE > page) {
    paginationNext[0].classList.remove("disabled");
    paginationNext[1].classList.remove("disabled");
  } else {
    paginationNext[0].classList.add("disabled");
    paginationNext[1].classList.add("disabled");
  }
}
