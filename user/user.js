const urlParams = new URLSearchParams(window.location.search);
const myParam = urlParams.get("userName");
const userImg = document.querySelector(".user-img");
const userName = document.querySelector(".user-name");
const userInfo = document.querySelector(".user-about");
const paginationPrev = document.querySelectorAll(".prev");
const paginationNext = document.querySelectorAll(".next");
const PER_PAGE = 10;
let page = 1;
let totalCount = 0;

fetch(`https://api.github.com/users/${myParam}`)
  .then((res) => res.json())
  .then((data) => {
    totalCount = data.public_repos;
    handlePagination();
    userImg.src = data.avatar_url;
    userName.innerText = data.login;
    userInfo.innerHTML = `
      ${data.name ? `<p>Name: ${data.name}</p>` : ""}
      ${data.company ? `<p>Company: ${data.company}</p>` : ""}
      ${data.email ? `<p>Email: ${data.email}</p>` : ""}
      ${data.followers ? `<p>Followers: ${data.followers}</p>` : ""}
      ${data.following ? `<p>Following: ${data.following}</p>` : ""}
      ${data.location ? `<p>Location: ${data.location}</p>` : ""}
      ${data.bio ? `<p>Bio: ${data.bio}</p>` : ""}
    `;
  });

async function getRepos() {
  const userData = await fetch(
    `https://api.github.com/users/${myParam}/repos?per_page=${PER_PAGE}&page=${page}`
  ).then((res) => res.json());

  const repos = document.querySelector(".repos-list");
  repos.innerHTML = "";
  const fragment = new DocumentFragment();
  if (userData) {
    userData.forEach((repo) => {
      const div = document.createElement("div");
      div.className = "repo-item";
      const repoItem = `
         <p>${repo.name}</p>
         <a href="${repo.html_url}" target="_blank">${repo.html_url}</a>
         `;
      div.innerHTML = repoItem;
      fragment.appendChild(div);
    });
    repos.appendChild(fragment);
  }
}

getRepos();

function navigatePage(n) {
  page += n;
  handlePagination();
  getRepos();
}

function handlePagination() {
  if (page > 1) {
    paginationPrev[0].classList.remove("disabled");
    paginationPrev[1].classList.remove("disabled");
  } else {
    paginationPrev[0].classList.add("disabled");
    paginationPrev[1].classList.add("disabled");
  }

  if (totalCount / PER_PAGE > page) {
    paginationNext[0].classList.remove("disabled");
    paginationNext[1].classList.remove("disabled");
  } else {
    paginationNext[0].classList.add("disabled");
    paginationNext[1].classList.add("disabled");
  }
}
