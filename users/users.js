const searchEl = document.querySelector(".search-input");
const paginationPrev = document.querySelectorAll(".prev");
const paginationNext = document.querySelectorAll(".next");
const storageSearchValue = localStorage.getItem("search-users");
const PER_PAGE = 20;
let page = localStorage.getItem("page-users") || 1;
let totalCount = 0;

if (storageSearchValue) {
  searchEl.value = storageSearchValue;
  getUsers(storageSearchValue);
}

function searchChange(e) {
  localStorage.setItem("search-users", e.target.value);
}

function onSearchUsers(e) {
  if (e.keyCode === 13) {
    const { value } = e.target;
    page = 1;
    localStorage.setItem("page-users", page);
    getUsers(value);
  }
}

async function getUsers(value) {
  const usersData = await fetch(
    `https://api.github.com/search/users?q=${value}&per_page=${PER_PAGE}&page=${page}`
  ).then((res) => res.json());

  totalCount = usersData.total_count;
  handlePagination();
  const usersList = document.querySelector(".users-list");
  usersList.innerHTML = "";
  const fragment = new DocumentFragment();

  if (usersData.items) {
    usersData.items.forEach((user) => {
      const div = document.createElement("div");
      div.className = "user-item";
      div.setAttribute("onclick", `onUserPage('${user.login}')`);
      const userElement = `
        <div class="avatar-box">
          <img src="${user.avatar_url}" alt="user" />
        </div>
        <p>${user.login}</p>
      `;
      div.innerHTML = userElement;
      fragment.appendChild(div);
    });

    usersList.appendChild(fragment);
  }
}

function onUserPage(name) {
  location.href = `../user/user.html?userName=${name}`;
}

function navigatePage(n) {
  const searchValue = localStorage.getItem("search-users");
  page = +page + n;
  localStorage.setItem("page-users", page);
  handlePagination();
  getUsers(searchValue);
}

function handlePagination() {
  if (page > 1) {
    paginationPrev[0].classList.remove("disabled");
    paginationPrev[1].classList.remove("disabled");
  } else {
    paginationPrev[0].classList.add("disabled");
    paginationPrev[1].classList.add("disabled");
  }

  if (totalCount / PER_PAGE > page) {
    paginationNext[0].classList.remove("disabled");
    paginationNext[1].classList.remove("disabled");
  } else {
    paginationNext[0].classList.add("disabled");
    paginationNext[1].classList.add("disabled");
  }
}
